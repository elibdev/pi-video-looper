#!/usr/bin/env bash

# This script should be run with sudo.

# Set up SSH by adding a blank file called "ssh"
echo "Setting up SSH..."
touch /Volumes/boot/ssh

# Set up avahi to use mDNS to show up as raspberrypi.local
echo "Setting up mDNS..."
touch /Volumes/boot/avahi

# Append "dtoverlay=dwc2" as a new line at end of the "/Volumes/boot/config.txt" to use a different driver that supports USB gadget mode
echo "Setting up USB gadget mode..."
echo "dtoverlay=dwc2" >> /Volumes/boot/config.txt 
# In the file "/Volumes/boot/cmdline.txt" edit "rootwait" to "rootwait modules-load=dwc2,g_ether"
sed -i -e 's/rootwait/rootwait modules-load=dwc2,g_ether/' /Volumes/boot/cmdline.txt

