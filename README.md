# Rasperry Pi Video Looper

This project is to create a Rasperry Pi that automatically loops a video fullscreen when it is powered on.

## Materials

- Pi
- PiTFT
- microSD card
- Pi power supply
- mac computer for preparing the microSD card

## How to Create a Raspberry Pi Video Looper OS Image

**NOTE: There is a copy of the Pi disk image at dozie-pi-diptych.dmg. You can set up a new SD card with just `dd`.**

1. Prepare a microSD card with Rasperry Pi OS installed using the [Raspberry Pi Imager][imager]. With your microSD card inserted, select the default OS (Raspberry Pi OS 32-bit), your microSD card, and then click "WRITE." This will begin the process of writing the OS image onto your SD card. This may take around 30 minutes. When it says it's complete, remove the SD card from your computer.

**NOTE:** As of the time of this writing the latest Raspbian OS doesn't work with PiTFT displays and an earlier version must be used. This is the latest image that was tested working with PiTFT (from adafruit blog post: https://learn.adafruit.com/adafruit-2-2-pitft-hat-320-240-primary-display-for-raspberry-pi/easy-install): https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2023-12-11/2023-12-11-raspios-bookworm-armhf-lite.img.xz

```
cd ~/Downloads
# download the SHA 256 checksum to verify the download
curl -O https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip.sha256
# download the OS image
curl -O https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip
# verify the checksum
sha256sum -c 2021-05-07-raspios-buster-armhf-lite.zip.sha256
# should display: 2021-05-07-raspios-buster-armhf-lite.zip: OK
# unzip the compressed image
unzip 2021-05-07-raspios-buster-armhf-lite.zip
```

Now after checking that your USB is in `/dev/disk2/` (it might be different, run `diskutil list` for more info) and is clear to be erased, run:
`sudo dd if=2021-05-07-raspios-buster-armhf-lite.img of=/dev/rdisk2 bs=1m`

PiTFT 2.2" guide: https://learn.adafruit.com/adafruit-2-2-pitft-hat-320-240-primary-display-for-raspberry-pi/easy-install

2. Insert the prepared microSD card in the Raspberry Pi. Connect a display via HDMI and a keyboard and mouse via USB. Then connect the Pi to a power source. Change the default password of the default pi user. Connect to a WiFi network and complete the installation.

3. Install [Adafruit display drivers][drivers]. Run the following code:

```
sudo apt-get install -y python3 python3-pip
cd ~
sudo pip3 install --upgrade adafruit-python-shell click==7.0
sudo apt-get install -y git
git clone https://github.com/adafruit/Raspberry-Pi-Installer-Scripts.git
cd Raspberry-Pi-Installer-Scripts
sudo python3 adafruit-pitft.py --display=28r --rotation=90 --install-type=fbcp
```

4. Copy the video file to Raspberry Pi in the `/home/pi/Videos/` folder.

5. Enter `sudo nano /etc/systemd/system/video-loop.service` and enter the following and save the file:

```
[Unit]
Description=Video Looper

[Service]
User=pi
Environment="DISPLAY=:0"
ExecStart=/usr/bin/vlc --loop --fullscreen --no-video-title-show /home/pi/Videos/
WorkingDirectory=/home/pi
Restart=always

[Install]
WantedBy=multi-user.target
```

6. Run this to enable to video looper service: `sudo systemctl enable video-loop`

## Set Up Via SSH over WiFi

1. Install Homebrew if you haven't already: `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. Install file system drivers: `brew cask install osxfuse ; brew install ext4fuse`
3. Insert your SD card with the Rasperry Pi OS image into your Mac. It should appear as a volume name "boot" at `/Volumes/boot/`.
4. Run `touch /Volumes/boot/ssh` to add an empty text file that will tell the Raspberry Pi to set up SSH access.
5. Run `nano /Volumes/boot/wpa_supplicant.conf` and enter the following text (replacing for your country code and WiFi name and password) and save:

```
country=US # replace with your country code
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
network={
    ssid="WIFI_NETWORK_NAME"
    psk="WIFI_PASSWORD"
    key_mgmt=WPA-PSK
}
```

Or alternatively use `nmcli` to set up a network connection interactively: https://www.linuxfordevices.com/tutorials/ubuntu/connect-wifi-terminal-command-line

Note that earlier models like Pi Zero W can't connect to 5GHz networks, only 2.4GHz.

You also might want to run `sudo apt update; sudo apt full-upgrade` to upgrade the packages.

6. Eject the SD card. Insert the SD card into the Pi and power it on.

## Set Up SSH over USB

These instructions are from [this blog post][pi-setup].

This setup doesn't require the Pi to have a WiFi connection.
You plug the Pi into your computer using a USB cable in Pi's USB port and SSH over the physical USB connection.
This requires you to set up the Pi to treat its USB connection as an internet connection, also called USB tethering.
These directions are for use on a Mac.

1. With the SD card plugged in the the Mac after burning the Raspberry Pi image, it will show up as a mounted external drive called "boot".
   You can configure the files in this volume to configure the Pi.

2. Run the following in a command line on your Mac with the "boot" volume mounted:

```
# Set up SSH by adding a blank file called "ssh"
sudo touch /Volumes/boot/ssh
# Set up avahi to use mDNS to show up as raspberrypi.local
sudo touch /Volumes/boot/avahi
# Append "dtoverlay=dwc2" as a new line at end of the "/Volumes/boot/config.txt"
echo "dtoverlay=dwc2" >> /Volumes/boot/config.txt
# In the file "/Volumes/boot/cmdline.txt" edit "rootwait" to "rootwait modules-load=dwc2,g_ether" to use a different driver that supports USB gadget mode
sed -i -e 's/rootwait/rootwait modules-load=dwc2,g_ether/' /Volumes/boot/cmdline.txt
```

You can also run `./setup_boot.sh` but you may need to modify the script first.

_You can optionally put some files on the Pi you want as well via the boot volume._

3. Eject the SD card, insert it into the Pi and connect it to your mac with the USB port that says "USB" over it, not the "PWR IN" one (for Pi 4 it's the USB-C port).
4. On your Mac, go to "System Preferences > Sharing" and turn on the "Internet Sharing" service from your WiFi to the computer named "RNDIS/Ethernet Gadget".
5. Run `ssh pi@raspberrypi.local` and enter your password when prompted. Now you're in.

## Set Up Samba File Sharing From the Pi

1. Install the Samba programs: `sudo apt update; sudo apt install samba samba-common-bin smbclient cifs-utils`.
2. Share your home folder by appending the following text to `/etc/samba/smb.conf`:

```
[share]
    path = /home/pi/
    read only = no
    public = yes
    writable = yes
```

You will also need to run `smbpasswd -a` when logged in via `ssh pi@raspberrypi.local` and then set the password for the "pi" user to log in as. It is not set by default.

3. Connect to the Pi from the Mac it's plugged into by going to "Network > Connect to Server" and entering `smb://raspberrypi.local` and connecting as Guest.

## Set Up the Video

Download a video. Here's a public domain [video of a train][video]. Place the video in the directory `/home/pi/Videos/` on the Pi.

_The rest of this document is currently a work in progress._

## Dual Displays

For using two LCD displays we'll need to connect the GPIO pins of the Pi to both displays.
The LCD displays like PiTFT use SPI (Serial Peripheral Interface) to communicate with the controller (Pi).
The SPI protocol has 4 pins and supports multiple devices.
Using the SS / CS (Chip Select) pin you can select tell only specific devices to listen.
The other 3 pins are MOSI (Master Out Slave In), MISO (Master In Slave Out), and SCLK (Serial Clock).
Those 3 pins should all go to both displays.
Since we are showing the same video on both displays we may not need the CS pin at all.

The Raspberry Pi and Orange Pi all have their SPI GPIO pins listed online.

To prototype the connections we can use a breadboard.

Once it's working we can use a perfboard to make it look cleaner.

The raspberry pi is set up with the username `pi` and the password `doziekanu`.

The two displays weren't working with the built-in adafruit display drivers so we are now using this one: https://github.com/juj/fbcp-ili9341

To set up the new driver first `git clone` the repo to the home folder. Then compile it:

```
cd /home/pi/fbcp-ili9341/build
sudo pkill fbcp-ili9341 ; rm CMakeCache.txt ; cmake -DSPI_BUS_CLOCK_DIVISOR=30 -DADAFRUIT_ILI9341_PITFT=ON -DSTATISTICS=0 -DDMA_TX_CHANNEL=8 -DDMA_RX_CHANNEL=5 .. ; make -j ; sudo cp fbcp-ili9341 /usr/local/sbin/ ; sudo systemctl daemon-reload ; sudo systemctl restart fbcp-ili9341
```

After installing the driver we should create a system service to always run it.

Enter `sudo nvim /etc/systemd/system/fbcp-ili.service` and enter the following and save the file:

```
[Unit]
Description="ILI 9341 TFT display driver"
After=network.target

[Service]
Type=simple
ExecStartPre=/bin/sleep 10
ExecStart=/home/pi/fbcp-ili9341/build/fbcp-ili9341
Restart=always

[Install]
WantedBy=multi-user.target
```

Run this to enable to video looper service: `sudo systemctl enable fbcp-ili`

To transfer a video to the device

## How to Use Orange Pi Instead of Raspberry Pi

Find the [armbian image][armbian] for your device.

We'll use Orange Pi Zero 2 and Orange Pi Lite.
Since Orange Pi Lite has a desktop supported OS let's try that one first.

There is info on burning an Armbian SD card here: https://docs.armbian.com/User-Guide_Getting-Started/#how-to-prepare-a-sd-card

Burning on an SD card on a Mac: https://superuser.com/questions/63654/how-do-i-burn-an-iso-on-a-usb-drive-on-mac-os-x

`diskutil list` to list your disks. When you plug in your SD card an extra disk should appear. Get the path of it.
Then run `diskutil unmounmtDisk /dev/disk2` using your SD card path as the final argument.
Then run `sudo dd if=Armbian_22.05.4_Orangepilite_jammy_current_5.15.48_xfce_desktop.img of=/dev/rdisk2 bs=1m`
The `rdisk` makes it faster.

You can hit CTRL+T to view the status of the transfer on the command line.

Once it is done, eject the card and insert it into the board and connect the power to boot it.

## Connect to the Orange Pi Lite

Plug a display and keyboard into the board with the HDMI and USB port. Connect power to the board.
You should see some small white text in the upper left of the display saying "Loading, please wait..." and a version number.

## Using Beaglebone Black

Download the latest Debian image compatible with Beaglebone Black: https://beagleboard.org/latest-images

[imager]: https://www.raspberrypi.org/software/
[drivers]: https://learn.adafruit.com/adafruit-pitft-28-inch-resistive-touchscreen-display-raspberry-pi/easy-install-2
[video]: https://www.pexels.com/video/a-bullet-train-traveling-fast-above-the-rail-tracks-3273637/
[pi-setup]: https://brandonb.ca/raspberry-pi-zero-w-headless-setup-on-macos
[armbian]: https://www.armbian.com/download/?device_support=Supported
